import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { RdStationService } from '../rd-station.service';


@Component({
  selector: "app-imc",
  templateUrl: "./imc.component.html",
  styleUrls: ["./imc.component.scss"]
})
export class ImcComponent implements OnInit {
  first = true;
  second = false;
  third = false;
  letra: string;
  rF: FormGroup;
  Rf: FormGroup;
  sub1 = false;
  sub2 = false;

  dados: any = {
    nome: "",
    email: "",
    fisica: "",
    genero: "",
    altura: "",
    peso: ""
  };

  abaixo = false;
  saudavel = false;
  sobrepeso = false;
  obeso = false;
  morbido = false;

  text: string;
  rodape: any;

  constructor(
    private fb: FormBuilder,
    public spinner: NgxSpinnerService,
    private rdStation: RdStationService,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    this.rF = this.fb.group({
      nome: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      fisica: new FormControl()
    });
    this.Rf = this.fb.group({
      genero: new FormControl('', Validators.required),
      peso: ['', Validators.required],
      altura: ['', Validators.required]
    });
  }

  get f() { return this.rF.controls; }
  get g() { return this.Rf.controls; }

  next() {
    if (this.rF.invalid) {
      return;
    }
    this.spinner.show();
    setTimeout(() => {
      this.dados.nome = this.rF.value.nome;
      this.dados.email = this.rF.value.email;
      this.dados.fisica = this.letter(this.rF.get('fisica').value);
      this.first = false;
      this.second = true;
      this.spinner.hide();
    }, 500);
  }

  calcular() {
    if (this.Rf.invalid) {
      return;
    }
    this.spinner.show();
    setTimeout(() => {
      this.first = false;
      this.second = false;
      this.third = true;
      this.dados.genero = this.Rf.get('genero').value;
      this.dados.altura = this.Rf.value.altura + ' centímetros';
      this.dados.peso = this.Rf.value.peso + ' quilos';
      this.dados.imc = this.round(this.imc(this.Rf.value.peso, this.Rf.value.altura), 1) + '%';
      this.bubble(this.imc(this.Rf.value.peso, this.Rf.value.altura));
      console.log('Dados', this.dados);
      this.spinner.hide();
    }, 1000);
  }


  imc(peso: number, altura: number) {
    if (altura <= 0) {
      this.toast.error('A altura não pode ser zero ou menor que zero', 'Erro');
      console.log('Erro');
    }
    altura = altura / 100;
    const total = peso / (altura * altura);
    total.toFixed(2);
    return total;
  }

  private round(value, precision) {
    const multiplier = Math.pow(10, precision || 0);
    return Math.round(value * multiplier) / multiplier;
  }

  private letter(item: any) {
    if (item === true) {
      this.letra = 'SIM';
    } else {
      this.letra = 'NÃO';
    }
    return this.letra;
  }

  private bubble(imc: any) {
    if (imc <= 19.0) {
      this.abaixo = true;
    }
    if (imc > 19.1 && imc <= 24.0) {
      this.saudavel = true;
    }
    if (imc > 24.1 && imc <= 29.0) {
      this.sobrepeso = true;
    }
    if (imc > 29.1 && imc <= 38.9) {
      this.obeso = true;
    }
    if (imc > 39) {
      this.morbido = true;
    }
  }
}
